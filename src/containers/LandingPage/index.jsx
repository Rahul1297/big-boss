import React, { Component } from 'react';
import './LandingPage.scss';
import axios from 'axios';
import images from '../../images'
import ProfileImage from '../../components/ProfileImage';


class LandingPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sampleArray: [1, 2, 3, 4],
      email: '',
      phone: '',
      emailValidation: '',
      phoneValidation: '',
    }
  }
  handleChange(newValue, id) {
    if (id === 'email') {
      this.setState({ email: newValue })
    }
    else if (id === 'phone') {
      this.setState({ phone: newValue })
    }
  }
  formValidation(value, id) {
    if (id === 'email') {
      let emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      let test = emailRegex.test(value);
      if (value === '') {
        this.setState({
          emailValidation: 'Email Should not be empty*'
        })
      }
      else if (!test) {
        this.setState({
          emailValidation: 'Please enter valid email*'
        })
      }
      else {
        this.setState({
          emailValidation: '',
        })
      }
    }
    if (id === 'phone') {
      if (value === '') {
        this.setState({
          phoneValidation: 'Phone number Should not be empty*'
        })
      }
      else {
        this.setState({
          phoneValidation: '',
        })
      }
    }
  }
  saveData() {
    axios.post('http://13.233.204.63:4000', {
      email: this.state.email,
      mobileNumber: this.state.phone
    })
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  }
  render() {
    return (
      <div className='wrapper'>
        <div className='section-1'>
          <div className='left-part'>
            <div className='top'>
              <h1>THE REALITY SOCIAL</h1>
            </div>
            <div className='middle'>
              <h1>EXPERIENCE BIG BOSS ON <span id='black'>WORLD'S</span> FIRST REALITY SOCIAL NETWORK</h1>
            </div>
            <div className='bottom'>
              <div className='vertical-line'></div>
              <div className='list'><div className='circle1'></div></div>
              <div className='list'><div className='circle'></div><h3>Real People</h3></div>
              <div className='list'><div className='circle'></div><h3>Real Drama</h3></div>
              <div className='list'><div className='circle'></div><h3>Real Game</h3></div>
            </div>
          </div>
          <div className='right-part'>
            <img className='rotate' src={images.path.logo} alt='' />
          </div>
        </div>
        <div className='section-2'>
          <div className='section-2-left'>
            <div className='heading'>
              <h1>A unique experience that involves the dynamics of social network and a social game</h1>
            </div>
            <div className='lists'>
              <div className='list-left'>
                <div className='list'><div className='circle'></div><h3>12 Strangers</h3></div>
                <div className='list'><div className='circle'></div><h3>Lies and Conflits</h3></div>
                <div className='list'><div className='circle'></div><h3>Aliances & Drama</h3></div>
                <div className='list'><div className='circle'></div><h3>Love & Betryals</h3></div>
                <div className='list'><div className='circle'></div><h3>Hate & Love</h3></div>
              </div>
              <div className='list-right'>
                <div className='list'><div className='circle'></div><h3>Challenges & Polls</h3></div>
                <div className='list'><div className='circle'></div><h3>Nomination & Evictions</h3></div>
                <div className='list'><div className='circle'></div><h3>Gossips & Backstabs</h3></div>
                <div className='list'><div className='circle'></div><h3>Anonymous players</h3></div>
                <div className='list'><div className='circle'></div><h3>Outrageous characters</h3></div>
              </div>
            </div>
          </div>
          <div className='section-2-right'>
            <img src={images.path.girl} alt='' />
          </div>
        </div>
        <div className='section-3'>
          <div className='section-3-top'>
            <div className='list'><h3>REAL PEOPLE</h3></div>
            <div className='list'><div className='circle'></div><h3>REAL DRAMA</h3></div>
            <div className='list'><div className='circle'></div><h3>REAL GAME</h3></div>
          </div>
          <div className='section-3-middle'>
            <div className='circle1'></div>
            <div className='vertical-line'></div>
            <div className='section-3-heading'>
              <h1>WE HAVE </h1>
              <h2>Found a new way to <span id='black'>Entertain you</span></h2>
            </div>
            <div className='section-3-content'>
              <div className='section-3-middle-left'>
                <div className='list'><div className='circle'><img src={images.path.hand} alt='' /></div><h3>Meet New Friends</h3></div>
                <div className='list'><div className='circle'><img src={images.path.home} alt='' /></div><h3>Create Your Own House</h3></div>
                <div className='list'><div className='circle'><img src={images.path.hand} alt='' /></div><h3>Meet New Friends</h3></div>
              </div>
              <div className='section-3-middle-right'>
                <div className='vertical-line'></div>
                <div className='list'><div className='circle'><img src={images.path.win} alt='' /></div><h3>Win Huge Prize</h3></div>
                <div className='list'><div className='circle'><img id='surprise' src={images.path.suprises} alt='' /></div><h3>Every Day New Suprises</h3></div>
                <div className='list'><div className='circle'><img src={images.path.win} alt='' /></div><h3>Win Huge Prize</h3></div>
              </div>
            </div>
          </div>
        </div>
        <div className='section-4'>
          <div className='section-4-heading'>
            <h1>After having a <span id='black'>grand success </span>
              with private comunity we are opening up to public</h1>
          </div>
          <div className='section-4-middle'>
            <div className='section-4-content'>
              <div className='users'>
                {this.state.sampleArray.map((ele, i) => {
                  return (
                    <div className='user-content'>
                      <ProfileImage imgSrc={images.path.userL} size={`60px`} />
                      <div className='user-details'>
                        <h4>Rahul</h4>
                        <h6>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'</h6>
                      </div>
                    </div>
                  )
                })}
              </div>
            </div>
            <div className='section-4-cup'>
              <img src={images.path.cup} alt='' />
            </div>
          </div>
        </div>
        <div className='section-5'>
          <div className='section-5-left'>
            <div className='section-5-heading'>
              <h1>About <span id='black'>us</span></h1>
            </div>
            <div className='section-5-content'>
              <p>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.
              </p>
              <p>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.
              </p>
              <p>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.
              </p>
            </div>
          </div>
          <div className='section-5-right'>

          </div>
        </div>
        <div className='section-6'>
          <div className='section-6-heading'>
            <h1>Launching all over the <span id='black'>World</span></h1>
          </div>
          <div className='section-6-content'>
            <div className='section-6-left'>
              <div className='subscribe-details'>
                <h5>SUBSRIBE FOR MORE DETAILS</h5>
                <div className='email'>
                  <label>Email ID:</label>
                  <div className='input-field'>
                    <input type='text'
                      id='email'
                      onChange={(event) => this.handleChange(event.target.value, event.target.id)}
                      onKeyUp={(event) => this.formValidation(event.target.value, event.target.id)}
                      value={this.state.email}
                      placeholder='enter your email address' />
                    <p>{this.state.emailValidation}</p>
                  </div>
                </div>
                <div className='phone'>
                  <label>Phone No.:</label>
                  <div className='input-field'>
                    <input type='text'
                      id='phone'
                      onChange={(event) => this.handleChange(event.target.value, event.target.id)}
                      onKeyUp={(event) => this.formValidation(event.target.value, event.target.id)}
                      value={this.state.phone}
                      placeholder='enter your phone number' />
                      <p>{this.state.phoneValidation}</p>
                  </div>
                </div>
                <button id='submit'>SUBMIT</button>
              </div>
            </div>
            <div className='section-6-right'>
              <div className='social-media-img'>
                <img src={images.path.fb} alt='' />
                <img src={images.path.wp} alt='' />
                <img src={images.path.twitter} alt='' />
                <img src={images.path.in} alt='' />
                <img src={images.path.pin} alt='' />
                <a href=''>All rights reserved</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default LandingPage;

