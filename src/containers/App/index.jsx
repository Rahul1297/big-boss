import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom'
import LoginPage from '../LoginPage'
import LandingPage from '../LandingPage'
import Registration from '../Registration'
import Redux from '../Redux'
import Chat from '../Chat'


function App() {

    return (
        <div>
            <main>
                <Route exact path="/" component={LandingPage} />
                <Route exact path="/login" component={LoginPage} />
                <Route exact path="/registration" component={Registration} />
                <Route exact path="/chat" component={Chat} />
                <Route exact path="/redux" component={Redux}/>
            </main>
        </div>
    );
}

export default App;

