import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import images from "../../images";
// import { connect } from 'react-redux';
import "./About.scss"


class About extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        return (
            <div >
                <Helmet>
                    <meta charSet="utf-8" />
                    <title>TSG</title>
                </Helmet>
                <div className="highlight">
                    <img src={images.path.avatar} alt=''></img>
                    All the Best with your 1st Project!
                </div>
            </div>
        );

    }
}

// const mapStateToProps = ({ }) => ({

// });
// export default connect(mapStateToProps, {})(About);

export default (About);
