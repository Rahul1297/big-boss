import React from 'react';
import './Loader.scss';
const LoadingSpinner = () => (
    <div className="loaderCss">
          <div>
    <i className="fa fa-spinner fa-spin" /> 
  </div>
    </div>

);

export default LoadingSpinner;

